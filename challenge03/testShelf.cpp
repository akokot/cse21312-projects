
#include <iostream>
#include "Shelf.h"

void loop(Shelf&);

int main() {
    Bottle A;
    Bottle L;
    Bottle R;
    A.setName("Azi");
    L.setName("Lisin");
    R.setName("Rou");

    Shelf pharm;
    /* std::cout << "at insert" << std::endl; */
    /* std::cout << pharm.shelf.head->data.peek() << " 1" << std::endl; */
    pharm.insert(R);
    /* std::cout << pharm.shelf.head->data.peek() << " 2" << std::endl; */
    pharm.insert(L);
    pharm.insert(A);
    /* std::cout << pharm.shelf.head->data.peek() << " 3" << std::endl; */
    loop(pharm);
    
}

void loop(Shelf& pharm) {
Node<Row<Bottle>>* t = pharm.shelf.head;
    while (t != nullptr && !t->data.empty()) {
        std::cout << t->data.peek() << std::endl;
        t = t->next;
    }
}
