/**********************************************
* File: testRowFinal.cpp
* Author: Algebros
* Email: rgipson2@nd.edu
* 
* Checks the <,>,==, and != operators for the Bottles
* in the Row structure.
**********************************************/

#include "Row.h"
#include <iostream>

/********************************************
* Function Name  : main
* Pre-conditions : none
* Post-conditions: int
* 
* Checks the comparison operators and prints
* the items that are true.
********************************************/
int main() {

    Bottle b1("Azithromycin", 45, Date(2019,1,23));
    Bottle b2("Lisinopril", 45, Date(2019,1,23));
    Bottle b3("Azithromycin", 50, Date(2019,1,23));

    Row<Bottle> Queue1; Queue1.add(b1);
    Row<Bottle> Queue2; Queue2.add(b2);
    Row<Bottle> Queue3; Queue3.add(b3);

    std::cout << "Queue1: " << Queue1.peek() << std::endl;
    std::cout << "Queue2: " << Queue2.peek() << std::endl;
    std::cout << "Queue3: " << Queue3.peek() << std::endl;

    if (Queue1 < Queue2) std::cout << "Queue1 < Queue2" << std::endl;
    if (Queue1 < Queue3) std::cout << "Queue1 < Queue3" << std::endl;
    if (Queue2 < Queue1) std::cout << "Queue2 < Queue1" << std::endl;
    if (Queue2 < Queue3) std::cout << "Queue2 < Queue3" << std::endl;

    if (Queue1 > Queue2) std::cout << "Queue1 > Queue2" << std::endl;
    if (Queue1 > Queue3) std::cout << "Queue1 > Queue3" << std::endl;
    if (Queue2 > Queue1) std::cout << "Queue2 > Queue1" << std::endl;
    if (Queue2 > Queue3) std::cout << "Queue2 > Queue3" << std::endl;

    if (Queue1 != Queue2) std::cout << "Queue1 != Queue2" << std::endl;
    if (Queue1 != Queue3) std::cout << "Queue1 != Queue3" << std::endl;
    if (Queue2 != Queue1) std::cout << "Queue2 != Queue1" << std::endl;
    if (Queue2 != Queue3) std::cout << "Queue2 != Queue3" << std::endl;

    if (Queue1 == Queue2) std::cout << "Queue1 == Queue2" << std::endl;
    if (Queue1 == Queue3) std::cout << "Queue1 == Queue3" << std::endl;
    if (Queue2 == Queue1) std::cout << "Queue2 == Queue1" << std::endl;
    if (Queue2 == Queue3) std::cout << "Queue2 == Queue3" << std::endl;

    return 0;
}
