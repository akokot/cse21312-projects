/*************************************
* File Name: Bottle.h
* Author: Alex Kokot, Kyle Duffy, Matt Schoenbauer, Robby Gipson
* Email: akokot@nd.edu kduffy5@nd.edu mschoenb@nd.edu rgipson2@nd.edu
*
* Header file that defines the Bottle class
* ***********************************/

#ifndef BOTTLE_H
#define BOTTLE_H

#include "Challenge01.h"

#include <string>
#include <ostream>
#include <iomanip>
#include <cctype>

class Bottle {

	public:
		Bottle(std::string, int, Date);
		Bottle():Name("No Name"),Number(0),ExpDate(Date(2019,1,1)){}
		Bottle(const Bottle&);
		~Bottle();
//		void operator= (const Bottle&);
		// std::string getName();
        std::string getName() const;
		Date getExpDate() const;
		int getNumber() const;
		void setName(std::string);
		void setExpDate(Date);
		void setNumber(int);
		void draw(int);
		bool isExpired(Date);
		bool isEmpty();
		void compare(std::ostream,Date);

		/*************************************
		* Function Name: operator<<
		* Preconditions: std::ostream&, Bottle
		* Post Conditions: std::ostream&
		*
		* Sends the name, number, and expiration date
		* of a bottle to an ostream, and returns the
		* ostream reference
		* ***********************************/

		friend std::ostream& operator<<(std::ostream& os, Bottle& bottle) {
			os << bottle.getName() << ". Pills = " << bottle.getNumber() << ". EXP: " << bottle.getExpDate();
			return os;
		}

		/*************************************
		* Function Name: operator>>
		* Preconditions: std::istream&, Bottle
		* Post Conditions: std::istream&
		*
		* Sends the information about the Bottle
		* to the istream and returns the istream
		* address
		* ***********************************/

		friend std::istream& operator>>(std::istream& file, Bottle& bottle) {
			std::string word;
			int year,month,day;
			file >> word;
			bottle.setName(word);
			file >> word;
			bottle.setNumber(std::atoi(word.c_str()));
			file >> word;
			year= std::atoi(word.c_str());
			file >> word;
			month=std::atoi(word.c_str());
			file >> word;
			day=std::atoi(word.c_str());
			bottle.setExpDate(Date(year, month, day));
/*	std::string Na = bottle.getName();
			int Nu = bottle.getNumber();
			Date Da = bottle.getExpDate();
			is >> Na >> Nu >> (Da).year >> (Da).month >> (Da).day;
*/
			return file;
		}

		/*************************************
		* Function Name: operator=
		* Preconditions: const Bottle &b
		* Post Conditions: void
		*
		* Assignment operator for the Bottle
		* class
		*
		* ***********************************/

		void operator= (const Bottle &b)
			{
				setName(Name);
				setNumber(Number);
				setExpDate(ExpDate);
			}

		/*************************************
		* Function Name: operator<
		* Preconditions: const Bottle &
		* Post Conditions: bool
		*
		* Compares alphabetical order of bottles
		* ***********************************/
                bool operator< (const Bottle& comp) {
                        std::string lhsName=this->getName();
                        std::string compName=comp.getName();
                        for(unsigned int i=0; i<lhsName.length() && i<compName.length();i++){
                                char letter1=tolower(lhsName[i]);
                                char letter2=tolower(compName[i]);
                                if(letter1>letter2) return false;
                                else if(letter1<letter2) return true;
                        }
                        if(lhsName.length()>=compName.length()) return false;
                        else return true;
                }
      		/*************************************
		* Function Name: operator>
		* Preconditions: const Bottle &
		* Post Conditions: bool
		*
		* Compares alphabetical order of bottles
		* ***********************************/
                bool operator> (const Bottle& comp) {
                        std::string lhsName=this->getName();
                        std::string compName=comp.getName();
                        for(unsigned int i=0; i<lhsName.length() && i<compName.length();i++){
                                char letter1=tolower(lhsName[i]);
                                char letter2=tolower(compName[i]);
                                if(letter1<letter2) return false;
                                else if(letter1>letter2) return true;
                        }
                        if(lhsName.length()>compName.length()) return true;
                        else return false;
                }
                /*************************************
		* Function Name: operator==
		* Preconditions: const Bottle &
		* Post Conditions: bool
		*
		* Compares alphabetical order of bottles
		* ***********************************/
                bool operator== (const Bottle& comp) {
                        std::string lhsName=this->getName();
                        std::string compName=comp.getName();
                        if(lhsName.length()!=compName.length()) return false;
                        for(unsigned int i=0; i<lhsName.length() && i<compName.length();i++){
                                char letter1=tolower(lhsName[i]);
                                char letter2=tolower(compName[i]);
                                if(letter1!=letter2) return false;
                        }
                        return true;
                }
                /*************************************
		* Function Name: operator!=
		* Preconditions: const Bottle &
		* Post Conditions: bool
		*
		* Compares alphabetical order of bottles
		* ***********************************/

                bool operator!= (const Bottle& comp) {
                        if(*this==comp) return false;
                        else return true;
                }
                                
	private:
		std::string Name;
		Date ExpDate;
		int Number;
};

/*************************************
* Function Name: Bottle
* Preconditions:
* Post Conditions:
*
* Default constructor for Bottle
*
*
* ***********************************/

//Bottle::Bottle()
//{
//	setName("No name");
//	setNumber(0);
//	Date d = Date(2019,1,1);
//	setExpDate(d);
//}

/*************************************
* Function Name: Bottle
* Preconditions: std::string, int, Date
* Post Conditions:
*
* Constructor for Bottle class.
* Allows for designated inputs
*
* ***********************************/

Bottle::Bottle(std::string Na, int Nu, Date da)
{
	setName(Na);
	setNumber(Nu);
	setExpDate(da);
}

/*************************************
* Function Name: ~Bottle
* Preconditions:
* Post Conditions:
*
* Destructor for Bottle class
*
*
* ***********************************/

Bottle::~Bottle()
{}

/*************************************
* Function Name: Bottle
* Preconditions: Bottle &
* Post Conditions:
*
* Creates a deep copy of a specified
* Bottle object
*
* ***********************************/

Bottle::Bottle(const Bottle& bottle): Name(bottle.getName()), Number(bottle.getNumber()), ExpDate(bottle.getExpDate())
{}

/*************************************
* Function Name: getName()
* Preconditions: void
* Post Conditions: std::string
*
* Allows the user to obtain the name
* of the Bottle object
*
* ***********************************/

std::string Bottle::getName() const
{
	return Name;
}

/*************************************
* Function Name: getExpDate()
* Preconditions: void
* Post Conditions: Date
*
* Allows the user to obtain the Date
* of the Bottle object
*
* ***********************************/

Date Bottle::getExpDate() const
{
	return ExpDate;
}

/*************************************
* Function Name: getNumber()
* Preconditions: void
* Post Conditions: int
*
* Allows the user to obtain the Number
* of the Bottle object
*
* ***********************************/

int Bottle::getNumber() const
{
	return Number;
}

/*************************************
* Function Name: setName()
* Preconditions: std::string
* Post Conditions: void
*
* Allows the user to set the Name
* of the Bottle object
*
* ***********************************/

void Bottle::setName(std::string Name)
{
	this->Name = Name;
}

/*************************************
* Function Name: setExpDate()
* Preconditions: Date
* Post Conditions: void
*
* Allows the user to set the ExpDate
* of the Bottle object
*
* ***********************************/

void Bottle::setExpDate(Date ExpDate)
{
	this->ExpDate = ExpDate;
}

/*************************************
* Function Name: setNumber()
* Preconditions: int
* Post Conditions: void
*
* Allows the user to set the Number
* of the Bottle object
*
* ***********************************/

void Bottle::setNumber(int Number)
{
	this->Number = Number;
}

/*************************************
* Function Name: draw
* Preconditions: int
* Post Conditions: void
*
* This function allows us to remove
* pills from the Bottle object
*
* ***********************************/

void Bottle::draw(int n)
{
	int m = getNumber();
	if(m - n < 0) setNumber(0);
	else{
		setNumber(m-n);
	}
}

/*************************************
* Function Name: isExpired
* Preconditions: Date
* Post Conditions: bool
*
* Compares the Date and the Bottle's
* expiration date. Returns true if
* expired and false otherwise.
* ***********************************/

bool Bottle::isExpired(Date d)
{
	return(getExpDate() <= d);
}

/*************************************
* Function Name: isEmpty
* Preconditions: void
* Post Conditions: bool
*
* returns true if the Number is 0
* and False otherwise
*
* ***********************************/

bool Bottle::isEmpty()
{
	return (getNumber() == 0);
}

/*************************************
* Function Name: compare
* Preconditions: std::ostream, Date
* Post Conditions: void
*
* Sends a message about whether the
* Bottle object is expired or not
* to the ostream
* ***********************************/

void Bottle::compare(std::ostream os,Date test)
{
    if (getExpDate() <= test) {
        os << "For Bottle " << getNumber() << ": The check date " << test << " is the same day or after the initial date " << getExpDate() << std::endl;
    } else {
        os << "For Bottle " << getNumber() << ": The check date " << test << " is before the initial date " << getExpDate() << std::endl;
    }
}




#endif
