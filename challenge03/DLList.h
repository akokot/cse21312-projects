/**********************************************
* File: DLList.h
* Author: Algebros
* Email: rgipson2@nd.edu
*  
*  Defines the doubly linked list and its member funcitons
**********************************************/
#ifndef DLLIST_H
#define DLLIST_H

#include <stdexcept>
#include <queue>
#include <iostream>
#include <string>
#include "Bottle.h"
#include "Challenge01.h"
#include "DLLNode.h"

template<class T>
class DLList  {

/********************************************
* Function Name  : operator<<
* Pre-conditions : std::ostream& stream, const DLList<T>& theList
* Post-conditions: std::ostream&
* 
* Output for the list
********************************************/
friend std::ostream& operator<< (std::ostream& stream, const DLList<T>& theList){
Node<T>* temp;
if (theList.head == NULL)
{
return stream;
}
temp = theList.head;
while (temp != NULL)
{
stream << temp->data << " " ;
temp = temp->next;
}
return stream;
}

	public:
		Node<T>* head;
		Node<T>* tail;


/********************************************
* Function Name  : DLList<T>
* Pre-conditions : none
* Post-conditions: none
* 
*constructor
********************************************/
DLList<T>()  {
	this->head = new Node<T>(); //create new Node of type T
	this->tail = head;
}


/********************************************
* Function Name  : ~DLList<T>
* Pre-conditions : none
* Post-conditions: none
* 
* Destructor
********************************************/
~DLList<T>() {
	Node<T>* current = head;

	while (current != nullptr)  {
		Node<T>* next = current->next;  // (2)
		delete current;  // (3)
		current = next;  // (4)
	}

	head = nullptr;
}


/********************************************
* Function Name  : insert
* Pre-conditions : T value
* Post-conditions: none
* 
* inserts a Node into the DLL
********************************************/
void insert(T value)  {
    tail->next = new Node<T>();  // Case 2: (1)
    tail->next->prev = tail;  // Case 2: (2)
    tail = tail->next;  // Case 2: (3)
    tail->data = value;  // Case 2: (4)
    tail->next =  nullptr;  // Case 2: (5)
}

/********************************************
* Function Name  : deleteNode
* Pre-conditions : T key
* Post-conditions: none
* 
* Deletes a node
********************************************/
void deleteNode(T key)  {
	if(head == nullptr)
		throw std::out_of_range("invalid LinkedList Node");

	else if(head->data == key)  {
		head = head->next;
		if(head != nullptr)
			head->prev = nullptr;
		return;
	}

	Node<T>* current = head;
	Node<T>* previous = nullptr;

	while(current != nullptr && current->data != key)  {
		previous = current;
		current = current->next;
	}

	previous->next = current->next;  // Case 3: (1)
	if(previous->next != nullptr) {
		previous->next->prev = previous;  // Case 3: (2)
	}
	if(current == tail)  {
		tail = nullptr;
		previous->next = tail;  // Cse 3: (3)
		tail = previous;   // Case 4: (4)
	}
	delete current;
}

};



#endif
