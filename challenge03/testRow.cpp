/**********************************************
* File: testRow.cpp
* Author: Algebros
* Email: rgipson2@nd.edu
*  
*  Performs various functions to test the Row 
*  data structure.
**********************************************/

#include <iostream>
#include <fstream>
#include <string>
#include <queue>
#include <list>
#include "Bottle.h"
#include "stack_queue.h"
#include "Row.h"

std::ifstream& getInput(std::string);
void put(Row<Bottle>&, Bottle&);

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char **argv
* Post-conditions: int
* 
* Reads in a txt file and performs functions 
* like STOCK, Inspection, and Fill prescription
********************************************/
int main(int argc, char **argv) {
    std::ifstream& is = getInput(argv[1]);
    Row<Bottle> row;

    while (!is.eof()) {
        std::string operation;
        is >> operation;
        if (operation == "STOCK") {
            Bottle b;
            is >> b;
            std::cout << "STOCK: Stocking new bottle: " << b << std::endl;
            put(row, b);
        } else if (operation == "INSPC") {
            Date day;
            is >> day;
            std::cout << "INSPECTION on " << day << std::endl;
            row.clean(day);
        } else if (operation == "SCRIP") {
            Date presDate;
            std::string pill;
            int num;
            is >> presDate >> pill >> num;
            std::cout << "SCRIPT! Patient brought in a prescription on " << presDate <<
                " for " << num << " " << pill << std::endl;
            row.dispense(pill, num, presDate);
        }
        std::cout << std::endl;
    }
    is.close();
}
/****************************
* Function name: getInput
* Preconditions: std::string
* Postconditions: void
* 
* Takes in an ifstream and opens the file indicated
* by the string and checks to ensure that it is good.
****************************/
std::ifstream& getInput(std::string fileName){
	static std::ifstream inputFile(fileName);
	if(inputFile.good()){
		return inputFile;
	} else {
        std::cout << "input file error" << std::endl;
		return inputFile;
	}
}

/*******************************
* Function name: dispense
* Preconditions: Row<Bottle>&, Bottle&
* Postconditions: none
*
* Compares the initial date and the test date,
* and displays an output message that indicates
* which comes first
*******************************/
void put(Row<Bottle>& row, Bottle& b) {
    if (row.empty() || row.peek().getName() == b.getName()) {
        row.add(b);
    } else {
        std::cout << "Pills don't match, cannot stock." << std::endl;
    }
}

