/**********************************************
* File: Shelf.h
* Author: Algebros
* Email: rgipson2@nd.edu
* 
* set-up for the Self Data Structure and its member 
* functions
**********************************************/
#ifndef SHELF_H
#define SHELF_H


#include <stdexcept>
#include <queue>
#include <iostream>
#include <string>
#include "Bottle.h"
#include "Challenge01.h"
#include "DLLNode.h"
#include "DLList.h"
#include "Row.h"
#include "Challenge01.h"

struct Shelf  {

    public:
        DLList<Row<Bottle>> shelf;

        /********************************************
        * Function Name  : insert
        * Pre-conditions : Bottle bottle
        * Post-conditions: none
        * 
        * inserts the bottle in the proper location
        ********************************************/
        void insert(Bottle bottle)  {
            Node<Row<Bottle>>* traversal = new Node<Row<Bottle>>();
            traversal = shelf.head;
            if (traversal->data.empty()) {
                traversal->data.add(bottle);
                return;
            }

            while (traversal != shelf.tail && traversal->data.peek() < bottle) {
                traversal = traversal->next;
            }
            bool placeBefore = traversal->data.peek() > bottle;

            if (traversal->data.peek() == bottle) {
                traversal->data.add(bottle);
            } else {
                Node<Row<Bottle>>* node = new Node<Row<Bottle>>;
                node->data.add(bottle);
                if (placeBefore) { 
                    node->prev = traversal->prev;
                    node->next = traversal;
                    traversal->prev = node;
                    if (traversal == shelf.head) {
                        shelf.head = node;
                    } else {
                        node->prev->next = node;
                    }

                } else {
                    shelf.insert(node->data);
                    shelf.tail = node;
                }
            }
        }

        /********************************************
        * Function Name  : clear
        * Pre-conditions : none
        * Post-conditions: none
        * 
        * clears the shelf from empty rows
        ********************************************/
        void clear()  {
            Node<Row<Bottle>>* traversal = new Node<Row<Bottle>>;
            traversal = shelf.head;

            while(traversal != nullptr && shelf.head->next != nullptr) {
                traversal = checkAndRip(traversal);
            }
        }

        /********************************************
        * Function Name  : clear
        * Pre-conditions : Date checkDate
        * Post-conditions: none
        *  
        *  clearns the shelf of expired bottles
        ********************************************/
        void clear(Date checkDate) {
            Node<Row<Bottle>>* traversal = new Node<Row<Bottle>>;
            traversal = shelf.head;

            while(traversal != nullptr) {
                traversal->data.clean(checkDate);
                traversal = checkAndRip(traversal);
            }
        }

        /********************************************
        * Function Name  : ripRow
        * Pre-conditions : Node<Row<Bottle>>* traversal
        * Post-conditions: none
        * 
        * Removes a row from the shelf
        ********************************************/
        void ripRow(Node<Row<Bottle>>* traversal) {
            if(traversal->data == shelf.head->data) {
                if (traversal->next == nullptr) {
                    traversal->data = Row<Bottle>();
                } else {
                    shelf.head = shelf.head->next;
                    shelf.head->prev = nullptr;
                }
            } else {
                traversal->prev->next = traversal->next;
                if (traversal->next != nullptr) traversal->next->prev = traversal->prev;
                else shelf.tail = traversal->prev;
            }
       }

       /********************************************
       * Function Name  : dispense
       * Pre-conditions : bool* check, std::string name, int num, Date presDate
       * Post-conditions: none
       * 
       * Dispenses the drugs from the appropriate bottle
       * and performs the necessary checks
       ********************************************/
       void dispense(bool* check, std::string name, int num, Date presDate) {
           Node<Row<Bottle>>* traversal = new Node<Row<Bottle>>;
           traversal = shelf.head;
           while (traversal->next != nullptr || traversal == shelf.head) {
               if (name == traversal->data.peek().getName()) {
                   traversal->data.dispense(check, name, num, presDate);
                   if(!(*check))
                       ripRow(traversal);
                   return;
               }
               traversal = traversal->next;
           }
       }

    private:
       /********************************************
       * Function Name  : checkAndRip
       * Pre-conditions : Node<Row<Bottle>>* med
       * Post-conditions: Node<Row<Bottle>>*
       * 
       * checks if the row is empty and removes it
       ********************************************/
       Node<Row<Bottle>>* checkAndRip(Node<Row<Bottle>>* med) {
           Node<Row<Bottle>>* nextRow = med->next;
           if(med->data.empty()) {
               ripRow(med);
           }
           return nextRow;
       }

};



#endif
