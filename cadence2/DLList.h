#ifndef DLLIST_H
#define DLLIST_H

#include <stdexcept>
#include "DLLNode.h"

template<class T>
class DLList  {

/*****************************
* Function Name: operator<<
* Pre-condtions: std::ostream&, const DLList<T>&, Node<T>*
*
* This is an overloaded input operator
* to insert data into a DLList
******************************/
friend std::ostream& operator<< (std::ostream& stream, const DLList<T>& theList){
Node<T>* temp;
if (theList.head == NULL)
{
return stream;
}
temp = theList.head;
while (temp != NULL)
{
stream << temp->data << " " ;
temp = temp->next;
}
return stream;
}

	public:
		Node<T>* head;
		Node<T>* tail;

/********************
* Function Name: DLList<T>() (default constructor)
* Pre-conditions: none
* Post-conditions: none
*
* The default constructor makes
* a list with a new head node
**********************/
DLList<T>()  {
	this->head = new Node<T>(); //create new Node of type T
}

/**********************
* Function Name: ~DLList<T>() (destructor)
* Pre-conditions: none
* Post-conditions: none
*
* Frees the memory of a DLList object
**********************/
~DLList<T>() {
	Node<T>* current = head;

	while (current != nullptr)  {
		Node<T>* next = current->next;  // (2)
		delete current;  // (3)
		current = next;  // (4)
	}

	head = nullptr;
}

/*************************
* Function Name: insert
* Pre-conditions: T (templated value)
* Post-conditions: none
*
* Adds another node to a DLList
*************************/
void insert(T value)  {
	if (head->next == NULL)  {
		// Because head = new Node<T>();
		tail = head;  // Case 1: (1)
		head->next = tail;  // Case 1: (2)
		head->data = value;  // Case 1: (3)
	}
	else  {
		tail->next = new Node<T>();  // Case 2: (1)
		tail->next->prev = tail;  // Case 2: (2)
		tail = tail->next;  // Case 2: (3)
		tail->data = value;  // Case 2: (4)
		tail->next =  nullptr;  // Case 2: (5)
	}
}

/************************
* Function Name: deleteNode
* Pre-conditions: T
* Post-conditions: none
*
* Deletes the node in the DLList
* with the desired data stored
************************/
void deleteNode(T key)  {
	if(head == nullptr)
		throw std::out_of_range("invalid LinkedList Node");

	else if(head->data == key)  {
		head = head->next;
		if(head != nullptr)
			head->prev = nullptr;
		return;
	}

	Node<T>* current = head;
	Node<T>* previous = nullptr;

	while(current != nullptr && current->data != key)  {
		previous = current;
		current = current->next;
	}

	previous->next = current->next;  // Case 3: (1)
	if(previous->next != nullptr) {
		previous->next->prev = previous;  // Case 3: (2)
	}
	if(current == tail)  {
		tail = nullptr;
		previous->next = tail;  // Cse 3: (3)
		tail = previous;   // Case 4: (4)
	}
	delete current;
}

};



#endif
