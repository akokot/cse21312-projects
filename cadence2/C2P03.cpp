#include "Supp.h"
#include "DLLNode.h"

/********************************************
* Function Name : main
* Pre-conditions : int argc, char **argv
* Post-conditions: int
*
* This is the main driver function. It will 
* run through the functions as designed in the
* cadence2 challenges.
********************************************/
int main(int argc, char **argv){

std::ifstream inputStream;
getFileStream(inputStream, argv[1]);

DLList<int> theDLList;

addNodes(inputStream, theDLList);

std::cout << theDLList << std::endl;

return 0;
}
