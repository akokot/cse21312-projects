/**********************************************
* File: Supp.h
* Author: Robert Gipson
* Email: rgipson2@nd.edu
*
* The header file for the Cadence 2 Problem 1 code
**********************************************/
#ifndef SUPP_H
#define SUPP_H

#include "DLLNode.h"
#include <iostream>
#include <fstream>
#include "DLList.h"

/********************************************
* Function Name  : getFileStream
* Pre-conditions : std::ifstream& stream, char* fileName
* Post-conditions: none
*
* This function takes in a file name, opens an ifstream
* and checks if the file is valid
********************************************/
void getFileStream(std::ifstream& stream, char* fileName);

/*******************************************
* Function Name : addNodes
* Pre-conditions : std::ifstream&, Node<int>*, Node<int>*, Node<int>*
* Post-conditions : none
*
* Inserts the data from the file into the data values
* of the nodes.
*******************************************/
void addNodes(std::ifstream&, Node<int>*, Node<int>*, Node<int>*);

/******************************************
* Function Name: setPtrs
* Pre-conditions: Node<int>*, Node<int>*, Node<int>*
* Post-condtions: none
*
* Sets the prev and next pointers for a node.
******************************************/
void setPtrs(Node<int>*, Node<int>*, Node<int>*);

/********************************************
* Function Name: printPtrs
* Pre-conditions: std::ostream&, Node<int>*
* Post-conditions: none
*
* This function prints out the data stored at a node,
* and at the prev and next nodes it point to.
*******************************************/

void printPtrs(std::ostream&, Node<int>* node);

/*******************************
* Function Name: addNodes
* Pre-conditions: std::ifstream&, DLList<int>&
* Post-conditions: none
*
* This function overloads addNodes
* and implements it for a DLList
*******************************/
void addNodes(std::ifstream& inputStream, DLList<int>& theList);
#endif
