/**********************************************
* File: C2P01.cpp
* Author: Robert Gipson
* Email: rgipson2@nd.edu
* 
* Part of Cadence 2 Problem 1, creates an ifstream
* and calls the function
**********************************************/
#include "Supp.h"

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char **argv
* Post-conditions: int
* 
* The main driver function. This function will execute
* the desired function in the cadence2 challenge.
********************************************/
int main(int argc, char **argv){
	std::ifstream inputStream;
	getFileStream(inputStream, argv[1]);
	return 0;
}
