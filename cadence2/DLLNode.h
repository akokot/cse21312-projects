#ifndef DLLNODE_H
#define DLLNODE_H

template<class T>
struct Node {
    T data;
    Node *next;
    Node *prev;
};

#endif
