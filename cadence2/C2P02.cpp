#include "Supp.h"
#include "DLLNode.h"

/**************************
* Function Name: main (main driver function)
* Pre-conditions: int, char **
* Post-conditions: int
*
* This is the main driver function.
* It will execute the desired functions
* as designed in the cadence2 challenge.
**************************/
int main(int argc, char **argv){
	std::ifstream inputStream;
	getFileStream(inputStream, argv[1]);

	Node<int>* node1 = new Node<int>();
	Node<int>* node2 = new Node<int>();
	Node<int>* node3 = new Node<int>();

	addNodes(inputStream, node1, node2, node3);

	setPtrs(node1, nullptr, node2);
	setPtrs(node2, node1, node3);
	setPtrs(node3, node2, nullptr);

	printPtrs(std::cout, node1);
	printPtrs(std::cout, node2);
	printPtrs(std::cout, node2); //Probably a typo in the assignment and should be node3

	return 0;
}
