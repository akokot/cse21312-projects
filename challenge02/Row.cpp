
#include "Row.h"

template <class Bottle>
void Row<Bottle>::dispense(int num, Date& presDate) {
    clean(presDate);                    // inspect BEFORE dispensing
    bool filled = false;
    int remainder;
    while (!empty() && !filled) {
        remainder = peek().getNumber(); // check how many pills remain
        if (num >= remainder) {          // cycle through bottles until all are dispensed
            num -= remainder;               // virtually dispense
            discard();                      // remove empty bottle
            remainder = peek().getNumber();
        } else {
            peek().draw(remainder);
            std::cout << peek() << std::endl;
            filled = true;
        }
    }
    if (!filled) {
        std::cout << "Warning: stock exhausted before prescription was filled, ";
        std::cout << num << " remaining in prescription." << std::endl;
    }
}

template<class Bottle>
void Row<Bottle>::clean(Date inspectionDate) {
    while (!empty() && peek().isExpired(inspectionDate)) { // TODO FIX isExpired()
        discard();
    }
    if (empty()) {
        std::cout << "No more bottles to inspect, out of stock." << std::endl;
    }
}
