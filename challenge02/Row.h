

#ifndef ROW_H
#define ROW_H

#include <queue>
#include <iostream>
#include <string>
#include "Bottle.h"
#include "Challenge01.h"

template <class T>
class Row {
	private:
		std::queue<T> row;
        std::string name;

	public:

		/*************************************
		* Function name: Row
		* Preconditions: std::string
		* Post Conditions:
		*
		* Default constructor for Row
		*
		*
		* ***********************************/

	    Row(std::string name = "no name")
        { this->name = name; }

		/*************************************
		* Function name: getName()
		* Preconditions: void
		* Post Conditions: std::string
		*
		* Returns the name of the Row
		*
		*
		* ***********************************/

        std::string getName()
        { return name; }

		/*************************************
		* function name: empty
		* preconditions: void
		* post conditions: bool
		*
		* returns a boolean telling whether
		* the row is empty
		*
		* ***********************************/

		bool empty()
		{ return row.empty(); }

		/*************************************
		* function name: add
		* preconditions: Bottle
		* post conditions: bool
		*
		* Pushes a bottle onto the queue
		*
		*
		* ***********************************/

		void add(Bottle b)
		{ row.push(b); }

		/*************************************
		* function name: peek
		* preconditions: void
		* post conditions: Bottle
		*
		* Returns the bottle on the front of the row
		*
		*
		* ***********************************/

		Bottle& peek() // returns a reference to save space and allow us to draw
		{ return row.front(); }

		/*************************************
		* function name: discard
		* preconditions: void
		* post conditions: void
		*
		* Removes the bottle on the top of the queue
		*
		*
		* ***********************************/

		void discard()
		{ row.pop(); }

		void dispense(std::string, int, Date&);
		void clean(Date&);
};

template <class Bottle>

/*************************************
* function name: dispense
* preconditions: std::string, int, Date&
* post conditions: void
*
* dispenses enough pills to fill the order
* of presecription and checks other bottles
* for sufficient pills
* ***********************************/

void Row<Bottle>::dispense(std::string name, int num, Date& presDate) {
    if (empty()) { std::cout << "Row empty, prescription cannot be filled" << std::endl; return; }

    bool filled = false;
    if (name == peek().getName()) {
        int remainder; // declaration of remainder must be outside the scope of while loop
        while (!empty() && !filled) {
            if (peek().isExpired(presDate)) {
                std::cout << "Front bottle " << name << " is expired as of " << peek().getExpDate()
                    << ". Removed from row." << std::endl;
                discard();
            } else {
                std::cout << "Front bottle of " << name << " is not expired." << std::endl;
                remainder = peek().getNumber(); // check how many pills remain
                if (num >= remainder) {          // cycle through bottles until all are dispensed
                    std::cout << "Prescription exceeds number of pills. Can only dispense "
                        << remainder << std::endl;
                    num -= remainder;               // virtually dispense
                    discard();
                    remainder = peek().getNumber();
                } else {
                    std::cout << "Currently have " << remainder << ", dispensing " << num << std::endl;
                    peek().draw(num);
                    filled = true;
                }
            }
        }
        if (!filled) {
            std::cout << "The pharmacy is out of " << name << " and the patient still needs "
                << num << std::endl;
        }
    } else {
        std::cout << "Wrong drug, cannot fill." << std::endl;
    }
}

template<class Bottle>

/*************************************
* function name: clean
* preconditions:  Date&
* post conditions: void
*
* iteratively checks bottles for expiry from
* front, removes expired bottles
*
* ***********************************/

void Row<Bottle>::clean(Date& inspectionDate) {
    if (empty()) {
        std::cout << "Row is empty! No bottles to inspect." << std::endl;
    } else {
        while (!empty() && peek().isExpired(inspectionDate)) {
            std::cout << peek() << " is expired! Discarding..." << std::endl;
            discard();
        }
        if (!empty()) {
             std::cout << "Bottle in front of " << peek().getName() << " with EXP: " <<
                 peek().getExpDate() << " passes inspection!!" << std::endl;
        }
    }
}

#endif
