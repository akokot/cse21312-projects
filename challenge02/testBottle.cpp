/*************************************
* File name: testBottle.cpp
* Author: Alex Kokot, Kyle Duffy, Matt Schoenbauer, Robby Gipson
* Email: akokot@nd.edu kduffy5@nd.edu mschoenb@nd.edu rgipson2@nd.edu
*
* Tests various operations of the Bottle
* class, and simulates a pharmacy with 
* only 2 bottles.
* ***********************************/


#include "Challenge01.h"
#include "Bottle.h"
#include <cstring>
#include <string>
#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>

void getInput(std::ifstream&,std::string);
void dispense(Bottle&, std::string, int, Date);
void display(std::ostream&,Date,Date);

/*************************************
* Function name: main
* Preconditions: int, char*
* Post Conditions: int
*
* Creates 2 bottles and uses the information
* given in the file indicated in the command
* line to perform various operations with them
* informing the user of the progess throughout.
* ***********************************/

int main (int argc, char* argv[]){
	Bottle bottle1("Azithromycin", 45, Date(2019,1,23));
	Bottle bottle2;
	std::cout << "bottle1: " << bottle1 << std::endl;
	std::cout << "bottle2: " << bottle2 << std::endl;
	std::ifstream file;
	getInput(file,argv[1]);
	std::string firstWord;
	std::string word;
	Date checkDate;
	std::string drugName;
	int numPills;
	while(!file.eof()){
		std::cout << std::endl;
		file >> firstWord;		
		if("STOCK"==firstWord){
			std::cout << "Stock:";
			file >> bottle2;
			std::cout << bottle2 << std::endl;
		}
		else if("INSPC"==firstWord){
			std::cout << "Inspection:" << std::endl;
			file >> checkDate;
			std::cout << "For Bottle 1: ";
			display(std::cout,bottle1.getExpDate(),checkDate);
			std::cout << "For Bottle 2: ";
			display(std::cout,bottle2.getExpDate(),checkDate);
		}
		else if("SCRIP"==firstWord){
			std::cout << "SCRIPT: ";
			file >> checkDate;
			file >> word;
			drugName= word;
			file >> word;
			numPills= std::atoi(word.c_str());
			dispense(bottle1, drugName, numPills, checkDate);
			dispense(bottle2, drugName, numPills, checkDate);
		}
	}
	file.close();
	return 0;
}

/*************************************
* Function name: getInput
* Preconditions: ifstream&, string
* Post Conditions: void
*
* Takes in an ifstream and opens the 
* file indicated by the string and 
* checks to ensure that it is good. 
* ***********************************/

void getInput(std::ifstream& inputFile,std::string s){
	inputFile.open(s);
	if(inputFile.good()){
	}
	else{
        std::cout << "input file error" << std::endl;
	}
}

/*************************************
* Function name: dispense
* Preconditions: Bottle, string, int, Date
* Post Conditions: Bottle
*
* Performs the functions and checks necessary
* to ensure the proper pateints get the
* correct drug and number of pills.
* Returns the bottle after use.
* ***********************************/
void dispense(Bottle &bottle, std::string name, int number, Date fillDate){
	if(bottle.getName()!=name){
	}

	else if(!(fillDate<=bottle.getExpDate())){
		std::cout << "Bottle of " << bottle.getName() << "is expired. It expired on " << bottle.getExpDate() << ". Cannot dispense pills." << std::endl;
	}

	else if(number>bottle.getNumber()){
		int remain=number-bottle.getNumber();
		std::cout << "Exceeded number of pills in the bottle. " << "Can only dispense " << bottle.getNumber() << ". " << remain << " pills remaining."  << std::endl;
		bottle.setNumber(0);
		std::cout << bottle << std::endl;
	}
	else{
		int start= bottle.getNumber();
		bottle.setNumber(start-number);
		std::cout << "Sufficient stock. Currently have " << start << " " << bottle.getName() << std::endl;
	       	std::cout << "Dispensing " << number << " " << bottle.getName() << std::endl;
		std::cout << bottle << std::endl;
	}
}

/*************************************
* Function name: display
* Preconditions: Date, Date
* Post Conditions: void
*
* Compares the initial date and the test date,
* and displays an output message that
* indicates which comes first
* ***********************************/
void display(std::ostream& os,Date initial,Date test){
	if (initial <= test) {
		os << "The check date " << test << " is the same day or after the initial date " << initial<< std::endl;
	} else {
		os << "The check date " << test << " is before the initial date " << initial<< std::endl;
	}
}
