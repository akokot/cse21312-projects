/*************************************
* File name: Challenge01.h
* Author: Alex Kokot, Kyle Duffy, Matt Schoenbauer, Robby Gipson
* Email: akokot@nd.edu kduffy5@nd.edu mschoenb@nd.edu rgipson2@nd.edu
*
* Header file that declares important data types and their members.
* ***********************************/

#ifndef CHALLENGE01_H
#define CHALLENGE01_H

#include <string>
#include <ostream>
#include <iomanip>

/* const array from which month names are accessed */
const std::string MONTHS[12] = { "JAN", "FEB", "MAR", "APR",
                                 "MAY", "JUN", "JUL", "AUG",
                                 "SEP", "OCT", "NOV", "DEC"  };

struct Date {

	/* Relevant member data */
	int year;
	int month;
	int day;

	/*************************************
	* Function name: Date
	* Preconditions: int,int,int
	* Post Conditions: void
	*
	* Constructs Date object, initializing members
	* ***********************************/
	Date(int year, int month, int day) {
		setMembers(year, month, day);
	}

	/*************************************
	* Function name: setMembers
	* Preconditions: int,int,int
	* Post Conditions: void
	*
	* Initializes/sets member data
	* ***********************************/
	void setMembers(int year, int month, int day) {
		this->year = year;
		this->month = month;
		this->day = day;
	}

	/*************************************
	* Function name: operator<<
	* Preconditions: ostream&, Date
	* Post Conditions: ostream&
	*
	* Prints a formatted dates to the ostream
	* object passed, typically cout. Formatted
	* as YYYY MMM DD
	* ***********************************/
	friend std::ostream& operator<<(std::ostream& os, Date date) {
		os << date.year << " " << MONTHS[date.month-1] << " " << std::setfill('0') << std::setw(2) << date.day;
		return os;
	}

	/*************************************
	* Function name: operator<
	* Preconditions: Date, Date
	* Post Conditions: bool
	*
	* Compares two Dates, returning true if
	* the first is on or before the second.
	* ***********************************/
	friend bool operator<=(Date date1, Date date2) {
		int comp1 = date1.year*10000 + date1.month*100 + date1.day; // Generates comparable statistic
		int comp2 = date2.year*10000 + date2.month*100 + date2.day; // (utilizes basic arithmetic for efficient comparison)
		return (comp1 <= comp2);
	}
};

#endif
