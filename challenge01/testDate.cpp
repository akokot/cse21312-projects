/*************************************
* File name: testDate.cpp
* Author: Alex Kokot, Kyle Duffy, Matt Schoenbauer, Robby Gipson
* Email: akokot@nd.edu kduffy5@nd.edu mschoenb@nd.edu rgipson2@nd.edu
*
* Test file that shows the comparison of dates
* ***********************************/

/* #include "Challenge01.h" */
#include "testhead.h"
#include <iostream>


void display(std::ostream&,Date,Date);

/*************************************
* Function name: main
* Preconditions: void
* Post Conditions: int
*
* Creates four Date objects and displays
* a comparison between the date of the
* initial Date object and the following
* three
* ***********************************/

int main() {
	/* Initializing Date objects */
	Date initial, before, same, after;
	initial.year = 2018;
	initial.month = 4;
	initial.day = 1;
	before.year = 1997;
	before.month = 3;
	before.day = 9;
	same.year = 2018;
	same.month = 4;
	same.day = 1;
	after.year = 2020;
	after.month = 3;
	after.day = 7;
	/* Displaying the date comparisons */
	display(std::cout,initial,before);
	display(std::cout,initial,same);
	display(std::cout,initial,after);
}

/*************************************
* Function name: display
* Preconditions: Date, Date
* Post Conditions: void
*
* Compares the initial date and the test date,
* and displays an output message that
* indicates which comes first
* ***********************************/

void display(std::ostream& os,Date initial,Date test){
	if (initial <= test) {
		os << "The check date " << test << " is the same day or after the initial date " << initial<< std::endl;
	} else {
		os << "The check date " << test << " is before the initial date " << initial<< std::endl;
	}
}
