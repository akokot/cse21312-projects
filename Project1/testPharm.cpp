/**********************************************
* File: testPharm.cpp
* Author: Algebros
* Email: rgipson2@nd.edu
*  
*  Tests the pharmacy data structure by perfroming 
*  various tasks with the bottles.
**********************************************/

#include <iostream>
#include <fstream>
#include <string>
#include <queue>
#include <list>
#include "Bottle.h"
#include "Row.h"
#include "Shelf.h"

std::ifstream& getInput(std::string);
/* void put(Row<Bottle>&, Bottle&); */
//dispense function is found in Row.h

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char **argv
* Post-conditions: int
* 
* Performs funciton like Stock, Inventory, fill
* prescription, and inspection based on the 
* text file read in.
********************************************/
int main(int argc, char **argv) {
    Shelf pharm;
    std::ifstream& is = getInput(argv[1]);
    Row<Bottle> row;
    bool check;
    while (!is.eof()) {
        check = true;
        std::string operation;
        is >> operation;
        if (operation == "STOCK") {
            Bottle b;
            is >> b;
            std::cout << "STOCK: Stocking new bottle: " << b << std::endl;
            pharm.insert(b); //This function completes all the necessary checks and insertions relevant to the insertion of the bottle into a row in the Shelf
            std::cout<< "Bottle inserted into proper row." << std::endl; 
        } else if (operation == "INSPC") {
            Date day;
            is >> day;
            std::cout << "INSPECTION on " << day << std::endl;            
            if(pharm.shelf.head->data.empty())
                std::cout << "Shelf is empty." << std::endl;
            else{
                pharm.clear(day);
            }
        } else if (operation == "SCRIP") {
            Date presDate;
            std::string pill;
            int num;
            is >> presDate >> pill >> num;
            std::cout << "SCRIPT! Patient brought in a prescription on " << presDate <<
                " for " << num << " " << pill << std::endl;
            pharm.dispense(&check, pill, num, presDate);
        } else if(operation == "INVEN"){
            std::cout << "Inven: Printing the inventory for the first bottle in each row..." << std::endl;
            Node<Row<Bottle>>* traversal=new Node<Row<Bottle>>;
            traversal=pharm.shelf.head;
            if(traversal->data.empty())
                std::cout<< "No medication to inspect." << std::endl;
            while(traversal!=nullptr && !traversal->data.empty()){
                std::cout << traversal->data.peek() << std::endl;
                traversal=traversal->next;
            }
        }
        std::cout << std::endl;
    }
    is.close();
}

/****************************
* Function name: getInput
* Preconditions: std::string
* Postconditions: void
* 
* Takes in an ifstream and opens the file indicated
* by the string and checks to ensure that it is good.
****************************/
std::ifstream& getInput(std::string fileName){
	static std::ifstream inputFile(fileName);
	if(inputFile.good()){
		return inputFile;
	} else {
        std::cout << "input file error" << std::endl;
		return inputFile;
	}
}

/*******************************
* Function name: put
* Preconditions: Row<Bottle>&, Bottle&
* Postconditions: none
*
* Compares the initial date and the test date,
* and displays an output message that indicates
* which comes first
*******************************/
/*
void put(Row<Bottle>& row, Bottle& b) {
    if (row.empty() || row.peek().getName() == b.getName()) {
        row.add(b);
    } else {
        std::cout << "Pills don't match, cannot stock." << std::endl;
    }
}
*/
