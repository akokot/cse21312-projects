/**********************************************
* File: Row.h
* Author: Algebros
* Email: rgipson2@nd.edu
*  
* Establishes the Row data structure and its member functions
**********************************************/


#ifndef ROW_H
#define ROW_H

#include <queue>
#include <iostream>
#include <string>
#include "Bottle.h"
#include "Challenge01.h"

template <class T>
class Row {
    private:
        std::queue<T> row;

    public:

        /*************************************
        * Function name: Row
        * Preconditions: std::string
        * Post Conditions:
        *
        * Default constructor for Row
        *
        *
        * ***********************************/

        Row()
        {}

        /*************************************
        * function name: empty
        * preconditions: void
        * post conditions: bool
        *
        * returns a boolean telling whether
        * the row is empty
        *
        * ***********************************/

        bool empty() const
        { return row.empty(); }

        /*************************************
        * function name: add
        * preconditions: Bottle
        * post conditions: bool
        *
        * Pushes a bottle onto the queue
        *
        *
        * ***********************************/

        void add(Bottle b)
        { row.push(b); }

        /*************************************
        * function name: peek
        * preconditions: void
        * post conditions: Bottle
        *
        * Returns the bottle on the front of the row
        *
        *
        * ***********************************/

        Bottle& peek() // returns a reference to save space and allow us to draww
        { return row.front(); }

        const Bottle& peek() const // marked const for calls on const Rows
        { return row.front(); }

        /*************************************
        * function name: discard
        * preconditions: void
        * post conditions: void
        *
        * Removes the bottle on the top of the queue
        *
        *
        * ***********************************/

        void discard()
        { row.pop(); }

        void dispense(bool*, std::string, int, Date&);
        void clean(Date&);
        bool operator< (const Row<T>& comp);
        bool operator> (const Row<T>& comp);
        bool operator== (const Row<T>& comp);
        bool operator!= (const Row<T>& comp);
};


/*************************************
* function name: dispense
* preconditions: bool*, std::string, int, Date&
* post conditions: void
*
* dispenses enough pills to fill the order
* of presecription and checks other bottles
* for sufficient pills
* ***********************************/

template <class Bottle>
void Row<Bottle>::dispense(bool* check, std::string name, int num, Date& presDate) {
    if (empty()) { std::cout << "Row empty, prescription cannot be filled" << std::endl; return; }

    bool filled = false;
    if (name == peek().getName()) {
        int remainder; // declaration of remainder must be outside the scope of while loop
        while (!empty() && !filled) {
            if (peek().isExpired(presDate)) {
                std::cout << "Front bottle " << name << " is expired as of " << peek().getExpDate()
                    << ". Removed from row." << std::endl;
                discard();
            } else {
                std::cout << "Front bottle of " << name << " is not expired." << std::endl;
                remainder = peek().getNumber(); // check how many pills remain
                if (num > remainder) {          // cycle through bottles until all are dispensed
                    std::cout << "Prescription exceeds number of pills. Can only dispense "
                        << remainder << std::endl;
                    num -= remainder;               // virtually dispense
                    discard();
                    remainder = peek().getNumber();
                } else if (num == remainder) {
                    std::cout << "Dispensing remainder of front bottle: " << peek() << std::endl;
                    num -= remainder;
                    filled = true;
                    discard();
                } else {
                    std::cout << "Currently have " << remainder << ", dispensing " << num << std::endl;
                    peek().draw(num);
                    filled = true;
                }
            }
        }
        if (empty()) *check = false;
        if (!filled) {
            std::cout << "The pharmacy is out of " << name << " and the patient still needs "
                << num << std::endl;
        }
    } else {
        std::cout << "Wrong drug, cannot fill." << std::endl;
    }
}


/*************************************
* function name: clean
* preconditions:  Date&
* post conditions: void
*
* iteratively checks bottles for expiry from
* front, removes expired bottles
*
* ***********************************/

template<class Bottle>
void Row<Bottle>::clean(Date& inspectionDate) {
    if (empty()) {
        std::cout << "Row is empty! No bottles to inspect." << std::endl;
    } else {
        while (!empty() && peek().isExpired(inspectionDate)) {
            std::cout << peek() << " is expired! Discarding..." << std::endl;
            discard();
        }
        if (!empty()) {
             std::cout << "Bottle in front of " << peek().getName() << " with EXP: " <<
                 peek().getExpDate() << " passes inspection!!" << std::endl;
        }
    }
}


template <class T>
/********************************************
* Function Name  : Row<T>::operator<
* Pre-conditions : const Row<T>& comp
* Post-conditions: bool
* 
* Overloaded less than operator returns T/F based on 
* the alphabetical order of the names of the bottles
* assigned to the rows
********************************************/
bool Row<T>::operator< (const Row<T>& comp) {
    return this->peek() < comp.peek();
}

template <class T>
/********************************************
* Function Name  : Row<T>::operator>
* Pre-conditions : const Row<T>& comp
* Post-conditions: bool
* Overloaded less than operator returns T/F based on 
* the alphabetical order of the names of the bottles
* assigned to the rows  
********************************************/
bool Row<T>::operator> (const Row<T>& comp) {
    return this->peek() > comp.peek();
}

template <class T>
/********************************************
* Function Name  : Row<T>::operator==
* Pre-conditions : const Row<T>& comp
* Post-conditions: bool
* 
* Overloaded less than operator returns T/F based on 
* the alphabetical order of the names of the bottles
* assigned to the rows  
 
********************************************/
bool Row<T>::operator== (const Row<T>& comp) {
    return this->peek() == comp.peek();
}

template <class T>
/********************************************
* Function Name  : Row<T>::operator!=
* Pre-conditions : const Row<T>& comp
* Post-conditions: bool
*
* Overloaded less than operator returns T/F based on 
* the alphabetical order of the names of the bottles
* assigned to the rows  

********************************************/
bool Row<T>::operator!= (const Row<T>& comp) {
    return this->peek() != comp.peek();
}

#endif
