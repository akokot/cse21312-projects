/**********************************************
* File: DLLNode.h
* Author: Algebros
* Email: rgipson2@nd.edu
*  
*  Defines the Node class for the DLL
**********************************************/
#ifndef DLLNODE_H
#define DLLNODE_H

template<class T>
struct Node {
    T data;
    Node *next;
    Node *prev;
};

#endif
